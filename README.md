# Summary:

This project is a result of qa assignment.
It represents the toolkit for testing of RESTful APIs provided by the https://jsonplaceholder.typicode.com

# Requirements:

* Python 3.6 and higher
* pip
* pytest
* jsonschema
* requests

# Test running:
1. Clone the repo:
```git clone https://gitlab.com/deim/qa_assignment.git```
2. Go to the project directory: 
```qa_assignment```
3. Install requirements:
```pip install -r requirements.txt```
4. Run the tests:
    * Without generation of report file and low verbosity: ```pytest```
    * Without generation of report file and high verbosity: ```pytest -vv```
    * With generation of HTML report and XML report so it can be parsed by Jenkins:
     ```pytest -vv --html=reports/report_$(date +%Y-%m-%d_%H:%M:%S).html --self-contained-html --junitxml=reports/xml/report.xml```
    * Choose what test to run:
    ```pytest -k filename.py``` - by filename
    ```pytest -k get``` - by pattern in test name.
    Also tests can be marked by some criteria and run by marks
# Project structure:
```
├── README.md
├── conftest.py          - configuration file
├── constants.py         - file containing constants
├── requirements.txt     - project requirements
├── routes           
│   └── requests.py      - wrapper over requests.request method
├── schemas
│   ├── schema_albums.py - schema of /albums
│   └── schema_users.py  - schema of /users
└── tests
    ├── test_albums.py   - tests for /albums
    └── test_users.py    - tests for /users
```

# Project description:

Required test tool was implemented using Python 3.6 programming language and Pytest testing framework.
Python was chosen because it's widely used, pretty simple and highly readable language with lots of libraries and wide community.
Pytest is a testing framework for Python language with constant support, good documentation, wide community and many tools out of the box.
During the implementation I used some external libraries and modules:
* requests - awesome library for making HTTP requests
* jsonschema - advanced lib for JSON schema validation
* pytest-html - module for building HTML reports

When running, pytest collects tests from the files with names 'test_*' and starts to execute 'test_*' methods inside.
During tests execution of tests, method make_request is calling from routes package and schemas is taken from schemas package.
Some tests are parametrized and same scenario executes with different input data. 
Amount of input data and other numerical constants is in constants.py file

# Test scenarios:
1. /albums
    * Get all albums `/albums` and make sure that there is only 100 of them, request is successful and schema is valid,
    * Get single one album `/albums/id` and make sure that there is only one album in result, request is successful,
    * Filter albums by userId `/albums?userId=user_id` and make sure that filter is working, request is successful
    * Change album title by `PUT` request and make sure that in response you got changed title, request is successful
2. /users
    * Get all users `/users` and make sure that there is only 10 of them, request is successful and schema is valid,
    * Get single one user `/users/id` and make sure that there is only one user in result, request is successful,
# Further improvements:

1. Improve schemas to validate patterns and make it more readable
2. More tests and negative scenarios (need some requirements and documentation to do so)
3. Better reporting (e.g. Allure framework)
4. Test marks to run separately
5. CI integration
