from constants import USERS_MAX_ITEMS, USERS_MIN_ITEMS

schema_users = {
    "type": "array",
    "additionalProperties": False,
    "minItems": USERS_MIN_ITEMS,
    "maxItems": USERS_MAX_ITEMS,
    "items": {
        "required": ["id",
                     "name",
                     "username",
                     "email",
                     "address",
                     "phone",
                     "website",
                     "company"
                     ],
        "properties": {
            "id": {
                "type": "integer",
                "title": "id of user"
            },
            "name": {
                "type": "string",
                "title": "name of user"
            },
            "username": {
                "type": "string",
                "title": "users username"
            },
            "email": {
                "type": "string",
                "title": "users email"
            },
            "address": {
                "type": "object",
                "title": "users address",
                "required": ["street",
                             "suite",
                             "city",
                             "zipcode",
                             "geo"],
                "properties": {
                    "street": {
                        "type": "string",
                        "title": "street name"
                    },
                    "suite": {
                        "type": "string",
                        "title": "suite number"
                    },
                    "city": {
                        "type": "string",
                        "title": "city name"
                    },
                    "zipcode": {
                        "type": "string",
                        "title": "zipcode"
                    },
                    "geo": {
                        "type": "object",
                        "title": "geo coordinates",
                        "required": ["lat",
                                     "lng"],
                        "properties": {
                            "lat": {
                                "type": "string",
                                "title": "latitude"
                            },
                            "lng": {
                                "type": "string",
                                "title": "longitude"
                            }
                        }
                    }

                }
            },
            "phone": {
                "type": "string",
                "title": "title of album"
            },
            "website": {
                "type": "string",
                "title": "users website"
            },
            "company": {
                "type": "object",
                "title": "users company",
                "required": ["name",
                             "catchPhrase",
                             "bs"],
                "properties": {
                    "name": {
                        "type": "string",
                        "title": "company name"
                    },
                    "catchPhrase": {
                        "type": "string",
                        "title": "catch phrase about the company"
                    },
                    "bs": {
                        "type": "string"
                    }
                }
            }
        }
    }
}
