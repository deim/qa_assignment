from constants import ALBUMS_MAX_ITEMS, ALBUMS_MIN_ITEMS

schema_albums = {
    "type": "array",
    "additionalProperties": False,
    "minItems": ALBUMS_MIN_ITEMS,
    "maxItems": ALBUMS_MAX_ITEMS,
    "items": {
        "required": ["userId",
                     "id",
                     "title"],
        "properties": {
            "userId": {
                "type": "integer",
                "title": "id of user"
            },
            "id": {
                "type": "integer",
                "title": "id of album"
            },
            "title": {
                "type": "string",
                "title": "title of album"
            }
        }
    }
}
