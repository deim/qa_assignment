ALBUMS_MIN_ITEMS = 1
ALBUMS_MAX_ITEMS = 100

USERS_MIN_ITEMS = 1
USERS_MAX_ITEMS = 10

SUCCESS_CODE = 200

PARAMS_NUMBER = 3  # amount of generated input data for parametrized tests

TITLE_LENGTH = 20  # amount of symbols to be generated
