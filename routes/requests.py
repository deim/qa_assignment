import pytest
import requests

from requests import HTTPError


def make_request(req_type,
                 route,
                 data=None
                 ):
    """
    :param req_type: (post, get, delete, put) - request method
    :param route: request destination
    :param data: request parameters
    :return: sends requests by the given route with given method and given data, returns response body
    """
    url = route if 'http' in route else f"http://{route}"
    try:
        if req_type == "GET":
            response = requests.request(req_type, url, params=data)
        else:
            response = requests.request(req_type, url, data=data)
        return response
    except HTTPError as e:
        pytest.fail(f"Request error: {e.response.text}, status code: {e.response.status_code}")
