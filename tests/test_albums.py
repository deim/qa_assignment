import pytest
import jsonschema
import random
import string

from routes.requests import make_request
from schemas.schema_albums import schema_albums
from conftest import ALBUMS_URL
from constants import *


def test_get_albums():
    """
    SUMMARY:  Test validates that /albums has expected amount of results, validates schema and checks if API is OK
    METHOD:   Send a GET request with required parameters.
    FAIL:     Length is not as expected, schema failed, status code is not 200 so API is not OK
    """
    response = make_request('GET', ALBUMS_URL)
    response_length = len(response.json())

    jsonschema.validate(response.json(), schema_albums)
    assert response_length == ALBUMS_MAX_ITEMS
    assert response.status_code == SUCCESS_CODE


@pytest.mark.parametrize('album_id', random.sample(range(ALBUMS_MIN_ITEMS, ALBUMS_MAX_ITEMS+1), PARAMS_NUMBER))
def test_get_one_album(album_id):
    """
    SUMMARY:  Test validates that /albums/{id} has expected id, and result is a single dict
    METHOD:   Send a GET request with required parameters.
    FAIL:     Id is not as expected, status code is not 200 so API is not OK, result is not a single dict
    """
    single_album_url = f"{ALBUMS_URL}/{album_id}"

    response = make_request('GET', single_album_url)
    response_album_id = response.json()['id']
    is_dict = isinstance(response.json(), dict)  # to make sure that we've got only one result

    assert response_album_id == album_id
    assert is_dict
    assert response.status_code == SUCCESS_CODE


@pytest.mark.parametrize('user_id', random.sample(range(USERS_MIN_ITEMS, USERS_MAX_ITEMS+1), PARAMS_NUMBER))
def test_get_album_by_user_id(user_id):
    """
    SUMMARY:  Test validates that /albums?userId=user_id filter is working
    METHOD:   Send a GET request with required parameters.
    FAIL:     set of userIds from response is not equal to single userId that is expected
    """
    album_by_id_url = f"{ALBUMS_URL}?userId={user_id}"
    response = make_request('GET', album_by_id_url)
    response_user_id = {album['userId'] for album in response.json()}   # get unique userIds from JSON

    assert response.status_code == SUCCESS_CODE
    assert response_user_id == {user_id}


@pytest.mark.parametrize('album_id', random.sample(range(ALBUMS_MIN_ITEMS, ALBUMS_MAX_ITEMS+1), PARAMS_NUMBER))
@pytest.mark.parametrize('new_title',
                         ["".join(random.choices(string.ascii_lowercase, k=TITLE_LENGTH)) for _ in range(PARAMS_NUMBER)])
def test_change_album_title(album_id, new_title):
    """
    SUMMARY:  Test validates that title of album is changed
    METHOD:   Send a PUT request with required data.
    FAIL:     status code is not 200, album title from response is not expected, changed album id is not expected
    """
    single_album_url = f"{ALBUMS_URL}/{album_id}"
    request_data = {"title": new_title}

    response = make_request('PUT', single_album_url, request_data)
    response_title = response.json()['title']
    response_album_id = response.json()['id']

    assert response.status_code == SUCCESS_CODE
    assert response_title == new_title
    assert response_album_id == album_id
