import pytest
import jsonschema
import random

from routes.requests import make_request
from schemas.schema_users import schema_users
from conftest import USERS_URL
from constants import *


def test_get_users():
    """
    SUMMARY:  Test validates that /users has expected amount of results, validates schema and checks if API is OK
    METHOD:   Send a GET request with required parameters.
    FAIL:     Length is not as expected, schema failed, status code is not 200 so API is not OK
    """
    response = make_request('GET', USERS_URL)
    response_length = len(response.json())

    jsonschema.validate(response.json(), schema_users)
    assert response_length == USERS_MAX_ITEMS
    assert response.status_code == SUCCESS_CODE


@pytest.mark.parametrize('user_id', random.sample(range(USERS_MIN_ITEMS, USERS_MAX_ITEMS+1), PARAMS_NUMBER))
def test_get_one_user(user_id):
    """
    SUMMARY:  Test validates that /users/{id} has expected id, and result is a single dict
    METHOD:   Send a GET request with required parameters.
    FAIL:     Id is not as expected, status code is not 200 so API is not OK
    """
    single_user_url = f"{USERS_URL}/{user_id}"

    response = make_request('GET', single_user_url)
    response_user_id = response.json()['id']
    is_dict = isinstance(response.json(), dict)  # to make sure that we've got only one result

    assert response_user_id == user_id
    assert is_dict
    assert response.status_code == SUCCESS_CODE
